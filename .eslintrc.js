module.exports = {
  //other base settings can be used from airBNB https://www.npmjs.com/package/eslint-config-airbnb
  //base setting is from CRA, which utilizes this plugins: ['import', 'flowtype', 'jsx-a11y', 'react', 'react-hooks', '@typescript-eslint'],
  //https://github.com/facebook/create-react-app/blob/master/packages/eslint-config-react-app/index.js

  plugins: ['prettier', 'testing-library', 'jest-dom', 'json'],
  extends: [
    'react-app', //based on create-react-app https://github.com/facebook/create-react-app/tree/master/packages/eslint-config-react-app
    'plugin:jsx-a11y/recommended', //based on create-react-app https://github.com/facebook/create-react-app/tree/master/packages/eslint-config-react-app
    'prettier/@typescript-eslint', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'plugin:testing-library/react',
    'plugin:jest-dom/recommended',
    'plugin:json/recommended',
    //has to be last!
    'plugin:prettier/recommended' // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  rules: {
    'jsx-a11y/no-autofocus': 'off' //https://www.samanthaming.com/tidbits/42-html5-autofocus/
  }
}
