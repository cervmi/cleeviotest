import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Trans, useTranslation } from 'react-i18next'
import { useLink } from 'valuelink'
import FormInput from '../../../components/formComponents/formInput'
import ToggleSwitch from '../../../components/formComponents/toggleSwitch'
import { addRequiredCheckToValueLink, addValidationCheck } from '../../../services/fields/validations'
import { FieldValidation } from '../../../services/requiredFields/fieldType'
import PageCard from '../../../components/Cards/pageCard'
import { usePageCardAlert } from '../../../components/Cards/pageCardAlert'
import { hasErrors } from '../../../services/formsServices/hasErrors'
import { InfoAlert } from '../../../components/formComponents'

export default function LoginComponent() {
  const { t } = useTranslation()
  const dataLinks = useLink({ email: '', password: '', remember: true }).pick('email', 'password', 'remember')
  const [showResetEmail] = useState(false)
  const pageCardAlert = usePageCardAlert()

  addRequiredCheckToValueLink(dataLinks.email, 'email')
  addRequiredCheckToValueLink(dataLinks.password, 'password')
  addValidationCheck(dataLinks.email, FieldValidation.isEmail)

  return (
    <PageCard pageCardAlert={pageCardAlert.alert} showLoader={false}>
      <div className="form-group">
        <div className="btn-list text-center">
          <a className="btn btn-facebook" href={'http://facebook.com'}>
            <i className="fa fa-facebook mr-2" />
            Facebook
          </a>
          <a className="btn btn-google" href={'http://google.com'}>
            <i className="fa fa-google mr-2" />
            Google
          </a>
        </div>
      </div>
      {showResetEmail && (
        <InfoAlert centeredText>
          <Trans i18nKey="login.emailNotConfirmed">
            Your email is not confirmed. Check your email or ,
            <a href="#javascript" role="button" onClick={handleResendConfirmationEmail}>
              &nbsp; resend confirmation email.
            </a>
          </Trans>
        </InfoAlert>
      )}
      <form onSubmit={handleLogin} noValidate>
        <FormInput label="E-mail" name="email" required valueLink={dataLinks.email} autoFocus />
        <FormInput
          label={t('formFields:password')}
          name="current-password"
          required
          isPassword
          valueLink={dataLinks.password}
          helperText={<Link to="/password-reset-request">{t('login.lostPasswordMessage')}</Link>}
        />
        <ToggleSwitch valueLink={dataLinks.remember} text={t('login.rememberMe')} />
        <div className="form-footer">
          <button type="submit" className="btn btn-primary btn-block" disabled={hasErrors(dataLinks)}>
            {t('login.login')}
          </button>
        </div>
      </form>
    </PageCard>
  )

  function handleLogin(event: any) {
    event.preventDefault()
  }

  function handleResendConfirmationEmail(e: any) {
    e.preventDefault()
  }
}
