import React from 'react'
import { useTranslation } from 'react-i18next'
import LoginComponent from './components/loginComponent'
import PageCard from '../../components/Cards/pageCard'

export default function Login() {
  const { t } = useTranslation()

  return (
    <div className="col col-login mx-auto">
      <PageCard headerTitle={t('login.login')}>
        <LoginComponent />
      </PageCard>
    </div>
  )
}
