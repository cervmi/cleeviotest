import React from 'react'
import LoginComponent from './components/loginComponent'
import { useTranslation } from 'react-i18next'
import Modal from '../../components/Modal/modal'

export default function LoginModal({ show, onClose }: { show: boolean; onClose: () => void }) {
  const { t } = useTranslation()
  return (
    <Modal show={show} onClose={onClose} title={t('login.login')} hideFooter confirmDisabled>
      <LoginComponent />
    </Modal>
  )
}
