import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLink } from 'valuelink'
import Recaptcha from '../../components/formComponents/recaptcha'
import FormInput from '../../components/formComponents/formInput'
import { addRequiredCheckToValueLink, addValidationCheck } from '../../services/fields/validations'
import { FieldValidation } from '../../services/requiredFields/fieldType'
import PageCard from '../../components/Cards/pageCard'

import { hasErrors } from '../../services/formsServices/hasErrors'
import { usePageCardAlert } from '../../components/Cards/pageCardAlert'

export default function PasswordReset() {
  const { t } = useTranslation()
  const pageCardAlert = usePageCardAlert()
  const dataLinks = useLink({ email: '', recaptchaValid: false }).pick('email', 'recaptchaValid')
  addRequiredCheckToValueLink(dataLinks.email, 'email')
  addValidationCheck(dataLinks.email, FieldValidation.isEmail)
  addValidationCheck(dataLinks.recaptchaValid, FieldValidation.isTrue)

  return (
    <div className="col col-login mx-auto">
      <PageCard headerTitle={t('login.passwordResetTitle')} showLoader={false} pageCardAlert={pageCardAlert.alert}>
        <form noValidate onSubmit={handleResetReq}>
          <FormInput label="E-mail" required valueLink={dataLinks.email} />
          <Recaptcha valueLink={dataLinks.recaptchaValid} />
          <div className="form-footer">
            <button type="submit" className="btn btn-primary btn-block" disabled={hasErrors(dataLinks)}>
              {t('login.pwdReset')}
            </button>
          </div>
        </form>
      </PageCard>
    </div>
  )

  function handleResetReq() {
    dataLinks.email.set('')
  }
}
