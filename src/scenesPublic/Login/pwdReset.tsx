import React, { useLayoutEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLink } from 'valuelink'
import { addRequiredCheckToValueLink, isStrongPassword } from '../../services/fields/validations'
import { WarningAlert } from '../../components/formComponents'
import FormInput from '../../components/formComponents/formInput'
import { getParamFromUrl } from '../../services/functions/getParamFromUrl'
import { isEmail } from 'linked-controls'
import PageCard from '../../components/Cards/pageCard'
import { hasErrors } from '../../services/formsServices/hasErrors'
import { usePageCardAlert } from '../../components/Cards/pageCardAlert'
import moment from 'moment'

export default function PasswordReset() {
  const { t } = useTranslation()

  const [inputParamsError, setInputParamsError] = useState('')
  const pageCardAlert = usePageCardAlert()
  const dataLinks = useLink({
    key: getParamFromUrl('key') ?? '',
    validTo: moment(getParamFromUrl('validto') ?? ''),
    email: getParamFromUrl('email') ?? '',
    pwd: ''
  }).pick('key', 'validTo', 'email', 'pwd')

  addRequiredCheckToValueLink(dataLinks.key, 'email')
  addRequiredCheckToValueLink(dataLinks.email, 'email')
  addRequiredCheckToValueLink(dataLinks.validTo, 'email')
  addRequiredCheckToValueLink(dataLinks.pwd, 'password')
  dataLinks.pwd.check(x => isStrongPassword(x), t('validationError.passwordHasToBeStrong'))

  useLayoutEffect(() => {
    if (!dataLinks.validTo.value || !isEmail(dataLinks.email.value) || !dataLinks.key.value) setInputParamsError(t('login.resetLinkIsWrong'))
    if (dataLinks.validTo.value.isBefore()) setInputParamsError(t('login.pwdResetValidationExpired'))
  }, [dataLinks.validTo.value, dataLinks.email.value, dataLinks.key.value, t])

  return (
    <div className="col col-login mx-auto">
      <PageCard headerTitle={t('login.passwordResetTitle')} showLoader={false} pageCardAlert={pageCardAlert.alert}>
        {inputParamsError && (
          <WarningAlert>
            <p>{inputParamsError}</p>
            <p>
              <a href="#javascript" role="button" onClick={handleResetReq}>
                {t('login.requestNewKey')}
              </a>
            </p>
          </WarningAlert>
        )}
        <form noValidate onSubmit={handleResetPwd}>
          <FormInput label={t('formFields:email')} valueLink={dataLinks.email} disabled />
          <FormInput label={t('formFields:password')} required={true} isPassword={true} valueLink={dataLinks.pwd} />
          <div className="form-footer">
            <button type="submit" className="btn btn-primary btn-block" disabled={hasErrors(dataLinks) || !!inputParamsError}>
              {t('login.updatePwd')}
            </button>
          </div>
        </form>
      </PageCard>
    </div>
  )

  function handleResetPwd() {}

  function handleResetReq() {}
}
