import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import './landingPage.scss'
import PageCard from '../../components/Cards/pageCard'
import SelectField from '../../components/formComponents/selectField'
import { IReactSelectItem } from '../../services/types/ReactSelect'
import { useLink } from 'valuelink'

const sourceUrl =
  'https://gist.githubusercontent.com/davidzadrazil/43378abbaa2f1145ef50000eccf81a85/raw/d734d8877c2aa9e1e8c1c59bcb7ec98d7f8d8459/countries.json'

type sourceDataType = {
  label: string
  data: { value: string; text: string; icon: string }[]
}

export default function LandingPage() {
  const { t } = useTranslation()

  const [countryList, setCountryList] = useState<IReactSelectItem[]>([])
  const selectedCountry = useLink('')

  useEffect(() => {
    fetch(sourceUrl)
      .then(response => response.json())
      .then((data: sourceDataType[]) => {
        setCountryList(data[0].data.map(x => ({ value: x.value, label: x.text })))
      })
  })
  return (
    <PageCard headerTitle={'Home'}>
      <h2>{t('startNewTrip')}</h2>
      <SelectField optionsData={countryList} valueLink={selectedCountry} isLoading={countryList.length <= 0} placeholder={t('selectCountry')}></SelectField>
    </PageCard>
  )
}
