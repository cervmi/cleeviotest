import React from 'react'
import RegisterComponent from './registerComponent'

export default function Register() {
  return (
    <div className="col col-login mx-auto">
      <RegisterComponent />
    </div>
  )
}
