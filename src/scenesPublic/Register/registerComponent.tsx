import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLink } from 'valuelink'
import { addRequiredCheckToValueLink, addValidationCheck, isStrongPassword } from '../../services/fields/validations'
import FormInput from '../../components/formComponents/formInput'
import ToggleSwitch from '../../components/formComponents/toggleSwitch'
import Recaptcha from '../../components/formComponents/recaptcha'
import { useHistory } from 'react-router-dom'
import { usePageCardAlert } from '../../components/Cards/pageCardAlert'
import { FieldValidation } from '../../services/requiredFields/fieldType'
import PageCard from '../../components/Cards/pageCard'

const emptyState = {
  email: '',
  password: '',
  firstname: '',
  lastname: '',
  companyIdentification: '',
  company: '',
  confirm: false,
  isSending: false,
  recaptchaValid: false,
  error: '',
  timezone: { timezone: Intl.DateTimeFormat().resolvedOptions().timeZone ?? 'Etc/GMT' }
}

export default function RegisterComponent({ registerCompany }: any) {
  const history = useHistory()
  const pageCardAlert = usePageCardAlert()
  const { t } = useTranslation()
  const dataLinks = useLink(emptyState).pick(
    'email',
    'password',
    'firstname',
    'lastname',
    'companyIdentification',
    'company',
    'confirm',
    'recaptchaValid',
    'timezone'
  )
  const isCompanyRegistration = registerCompany || history.location.state === 'company' //window.history.state.state === 'company'

  addRequiredCheckToValueLink(dataLinks.email, 'email')
  addValidationCheck(dataLinks.email, FieldValidation.isEmail)
  addRequiredCheckToValueLink(dataLinks.password, 'password')
  dataLinks.password.check(x => isStrongPassword(x), t('validationError.passwordHasToBeStrong'))
  dataLinks.confirm.check(x => x, t('validationError.agreeWithTermsError'))
  dataLinks.recaptchaValid.check(x => x)
  if (isCompanyRegistration) {
    addRequiredCheckToValueLink(dataLinks.companyIdentification, 'identificationNumber')
    addRequiredCheckToValueLink(dataLinks.company, 'company')
  } else {
    addRequiredCheckToValueLink(dataLinks.firstname, 'firstname')
    addRequiredCheckToValueLink(dataLinks.lastname, 'lastname')
  }

  return (
    <PageCard headerTitle={t('login.createNewAccountTitle')} showLoader={false} pageCardAlert={pageCardAlert.alert}>
      <form noValidate>
        {!isCompanyRegistration && (
          <React.Fragment>
            <FormInput label={t('formFields:firstname')} name="firstname" required valueLink={dataLinks.firstname} />
            <FormInput label={t('formFields:lastname')} name="lastname" required valueLink={dataLinks.lastname} />
          </React.Fragment>
        )}

        <FormInput label="E-mail" name="email" required={true} valueLink={dataLinks.email} />
        <FormInput label={t('formFields:password')} required={true} isPassword={true} valueLink={dataLinks.password} />
        <ToggleSwitch valueLink={dataLinks.confirm} required={true} text={t('login.confirmTerms')} />
        <Recaptcha valueLink={dataLinks.recaptchaValid} />
        <div className="form-footer">
          <button type="submit" onClick={handleRegistration} className="btn btn-primary btn-block" disabled={Link.hasErrors(dataLinks)}>
            {t('login.registerButton')}
          </button>
        </div>

        {isCompanyRegistration && (
          <React.Fragment>
            <hr />
            <div className="form-group mb-6">
              <div className="btn-list" style={{ textAlign: 'center' }}>
                <a className="btn btn-facebook" href={'http://facebook.com'}>
                  <i className="fa fa-facebook mr-2" />
                  Facebook
                </a>
                <a className="btn btn-google" href={'http://google.com'}>
                  <i className="fa fa-google mr-2" />
                  Google
                </a>
              </div>
            </div>
          </React.Fragment>
        )}
      </form>
    </PageCard>
  )

  function handleRegistration() {
    alert('registered')
  }
}
