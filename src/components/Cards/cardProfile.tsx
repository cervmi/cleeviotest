import React from 'react'
import { useTranslation } from 'react-i18next'
import { Avatar } from '../appComponents/avatar'

export function CardProfile({
  isCompanyTypeLogin,
  firstname,
  lastname,
  company,
  email,
  isNotMe = false
}: {
  isCompanyTypeLogin: boolean
  firstname?: string
  lastname?: string
  company?: string
  email: string
  isNotMe: boolean
}) {
  const { t } = useTranslation()
  const nameToShow = (firstname ?? '' + lastname).length > 0 ? `${firstname} ${lastname}` : company ? company : email
  return (
    <section className="card card-profile">
      <header
        className="card-header"
        style={{
          backgroundImage: 'url(/images/eberhard-grossgasteiger-311213-500.jpg)'
        }}
      />
      <div className="card-body text-center">
        <Avatar name={nameToShow} large={true} isNotMe={isNotMe} />
        <h3 className="mb-1">{`${firstname || ''} ${lastname || ''}`}</h3>
        {!firstname && !lastname && <h4 className="mb-3">{email}</h4>}
        <p className="mb-4">{isCompanyTypeLogin ? company : t('myProfile.endCustomer')}</p>
        <p className="mb-4">{isCompanyTypeLogin ? t('myProfile.companyProfile') : ''}</p>
      </div>
    </section>
  )
}
