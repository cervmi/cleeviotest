import React from 'react'
import { useHistory } from 'react-router-dom'

export const pageCardHeaderButton = (label: string, action: any) => (
  <button type="button" className="btn btn-primary btn-block btn-sm" onClick={action}>
    {' '}
    {label}
  </button>
)

export function PageCardLinkHeaderButton(label: string, url: string) {
  const history = useHistory()
  return (
    <button type="button" className="btn btn-primary btn-block btn-sm" onClick={() => history.push(url)}>
      {' '}
      {label}
    </button>
  )
}

export function PageCardLinkHeaderText(label: string, url: string) {
  return (
    <a href={url} target="_blank" rel="noopener noreferrer">
      {' '}
      {label}
    </a>
  )
}
