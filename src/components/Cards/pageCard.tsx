import React, { useRef, useState } from 'react'
import { Alert, WarningAlert } from '../formComponents'
// @ts-ignore
import { Animate } from 'react-show'
import { IPageCardAlert } from './pageCardAlert'
import { useTranslation } from 'react-i18next'
import { StateRef } from 'valuelink'
import { IReactSelectItem } from '../../services/types/ReactSelect'
import SelectGroup from '../formComponents/selectGroup'
import LoadingContainer from '../LoadingContainer'

type PageCardType = {
  headerTitle?: string | React.ReactElement
  bodyTitle?: string
  showLoader?: boolean
  pageCardAlert?: IPageCardAlert
  pageCardClass?: string
  cardOptions?: React.ReactElement
  cardTabs?: { link: StateRef<string>; options: IReactSelectItem[] }
  children?: React.ReactNode
}

function scrollTo(y: number) {
  window.scrollTo(0, y)
}

export default function PageCard({ headerTitle, bodyTitle, showLoader, pageCardAlert, pageCardClass, cardOptions, children, cardTabs }: PageCardType) {
  const { t } = useTranslation()
  const refTopOfCard = useRef<HTMLElement>(null)
  const [storedPageCardAlert, setStoredPageCardAlert] = useState(pageCardAlert)

  if (pageCardAlert && storedPageCardAlert !== pageCardAlert && pageCardAlert.visible && pageCardAlert.message) {
    setStoredPageCardAlert(pageCardAlert)
    if (refTopOfCard.current && refTopOfCard.current.getBoundingClientRect().top < 0) {
      const top = Math.floor(refTopOfCard.current?.getBoundingClientRect().top + window.scrollY)
      scrollTo(top)
    }
  }

  const customPageCardClass = pageCardClass ? pageCardClass : 'card'
  return (
    <section className={customPageCardClass} ref={refTopOfCard}>
      {headerTitle && (
        <>
          <div className="card-status bg-primaryColor" />
          <header className="card-header">
            <h1 className="card-title">{headerTitle}</h1>
            {cardTabs && <SelectGroup valueLink={cardTabs.link} inline={true} options={cardTabs.options} />}
            {cardOptions && <div className="card-options">{cardOptions}</div>}
          </header>
        </>
      )}
      {pageCardAlert && (
        <Animate show={pageCardAlert.visible} transitionOnMount style={{ height: 'auto', overflow: 'hidden' }} start={{ height: '0px', overflow: 'hidden' }}>
          <Alert type={pageCardAlert.type} message={pageCardAlert.message} />
        </Animate>
      )}

      <LoadingContainer showLoader={!!showLoader}>
        <article className="card-body">
          {bodyTitle && <h3 className="card-title">{bodyTitle}</h3>}
          {pageCardAlert?.errorList && pageCardAlert.errorList.length > 0 && <WarningAlert message={errorListForDisplay(pageCardAlert?.errorList)} />}
          {children}
        </article>
      </LoadingContainer>
    </section>
  )

  function errorListForDisplay(errorList: string[]) {
    return (
      <React.Fragment>
        <strong>{t('correctTheseErrors')}</strong>
        <ul>
          {errorList.map((x: any, index: number) => (
            <li key={`${x}${index}`}>{t('mutationResult:errorList.' + x)}</li>
          ))}
        </ul>
      </React.Fragment>
    )
  }
}
