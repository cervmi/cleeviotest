import React from 'react'
import LoadingContainer from '../LoadingContainer'
import { useHistory } from 'react-router-dom'
import { IStringObject } from '../../services/types/IStringObject'

export function StatsCard({
  caption,
  value,
  subValue,
  loading,
  linkTo
}: {
  caption: string
  value: string
  subValue?: string
  loading?: boolean
  linkTo?: string
}) {
  const history = useHistory()

  const onClickSupport: IStringObject = linkTo
    ? {
        onClick: () => {
          history.push(linkTo)
        },
        style: { cursor: 'pointer' }
      }
    : {}

  return (
    <div className="card" {...onClickSupport}>
      <LoadingContainer showLoader={!!loading}>
        <div className="card-body p-3 text-center">
          <div className="h5 m-0">{caption}</div>
          <div className="display-4 font-weight-bold">{value}</div>
          {subValue && <div className="text-muted">{subValue}</div>}
        </div>
      </LoadingContainer>
    </div>
  )
}
