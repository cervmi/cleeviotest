import { alertType } from '../formComponents'
import { useCallback, useEffect, useState } from 'react'

export const DEFAULT_ALERT_TIMEOUT = 8000

export interface IPageCardAlert {
  type: alertType
  message: string
  visible: boolean
  errorList?: string[]
}

export interface IPageCardShowAlert {
  type: alertType
  message: string
}

const initialState: IPageCardAlert = {
  type: alertType.primary,
  message: '',
  visible: false,
  errorList: []
}

export interface IPageCardAlertHook {
  alert: IPageCardAlert
  setAlert: (alert: IPageCardShowAlert) => void
  clearAlert: () => void
  setErrorList: Function
}

export function usePageCardAlert(): IPageCardAlertHook {
  const [pageCardAlert, setPageCardAlert] = useState<IPageCardAlert>(initialState)
  const [timeoutId, setTimeoutId] = useState<any>(null)

  useEffect(() => {
    function resetTimeout() {
      if (timeoutId) {
        clearTimeout(timeoutId)
      }
    }

    return resetTimeout
  }, [timeoutId])

  const setAlertCB = useCallback(setAlert, [])
  const clearAlertCB = useCallback(clearAlert, [])
  const setErrorListCB = useCallback(setErrorList, [])

  return { alert: pageCardAlert, setAlert: setAlertCB, clearAlert: clearAlertCB, setErrorList: setErrorListCB }

  function hideAlert() {
    setTimeoutId(null)
    setPageCardAlert(prevState => ({ ...prevState, visible: false }))
  }

  function clearAlert() {
    setPageCardAlert({ ...initialState })
  }

  function setAlert(alert: IPageCardShowAlert) {
    setPageCardAlert({ ...alert, visible: true, errorList: [] })
    const tmid = setTimeout(hideAlert, DEFAULT_ALERT_TIMEOUT)
    setTimeoutId(tmid)
  }

  function setErrorList(errorList: string[]) {
    setPageCardAlert(prevState => ({ ...prevState, errorList: errorList }))
  }
}
