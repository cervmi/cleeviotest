import React from 'react'
import { useTranslation } from 'react-i18next'

export default function P404() {
  const { t } = useTranslation()
  return (
    <div className="page">
      <div className="page-content">
        <div className="container text-center">
          <div className="display-1 text-muted mb-5">
            <i className="si si-exclamation" />
            404
          </div>
          <h1 className="h2 mb-3">{t('errorPage.404Title')}</h1>
          <p className="h4 text-muted font-weight-normal mb-7">{t('errorPage.404Message')}</p>
          <a className="btn btn-primary" href="/">
            <i className="fe fe-arrow-left mr-2" />
            {t('errorPage.goToHome')}
          </a>
        </div>
      </div>
    </div>
  )
}
