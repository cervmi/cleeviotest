import React, { useCallback, useEffect } from 'react'
import { useTranslation } from 'react-i18next'

export default function Modal({
  show,
  hideFooter = false,
  onClose,
  confirmLabel,
  onConfirm,
  title,
  confirmDisabled = false,
  showBackButton = false,
  children
}: ModalType) {
  const { t } = useTranslation()
  const handleEventListenerCB = useCallback(handleEventListener, [])
  useEffect(() => {
    if (show) document.addEventListener('keydown', handleEventListenerCB)
    return () => {
      document.removeEventListener('keydown', handleEventListenerCB)
    }
  }, [show, handleEventListenerCB])
  if (!show) return null

  return (
    <>
      <div className={'modal fade show'} tabIndex={-1} role="dialog" style={{ display: 'block' }}>
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                <div>
                  <img src="/images/logo.png" className="header-brand-img" alt="logo" />
                  {title}
                </div>
              </h5>
              {onClose && <button type="button" className="close" onClick={exit} />}
            </div>
            <div className="modal-body">{children}</div>
            {!hideFooter && (
              <div className="modal-footer">
                {showBackButton && onClose && (
                  <button type="button" className="btn btn-secondary" onClick={exit}>
                    {t('common:back')}
                  </button>
                )}
                <button type="button" className="btn btn-info" onClick={confirm} disabled={confirmDisabled}>
                  {confirmLabel}
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="modal-backdrop fade show" style={{ display: 'block' }} />
    </>
  )

  function exit() {
    if (onClose) {
      document.removeEventListener('keydown', handleEventListener)
      onClose()
    }
  }

  function confirm() {
    if (onConfirm && !confirmDisabled) {
      document.removeEventListener('keydown', handleEventListener)
      onConfirm()
    }
  }

  function handleEventListener(event: any) {
    if (event.key === 'Escape') exit()
    if (event.key === 'Enter') confirm()
  }
}

type ModalType = {
  show: boolean
  hideFooter?: boolean
  onClose?: () => void
  confirmLabel?: string
  confirmDisabled?: boolean
  onConfirm?: () => void
  title?: string
  showBackButton?: boolean
  children: React.ReactNode
}
