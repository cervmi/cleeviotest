import React from 'react'

type LoadingContainerParams = {
  showLoader: boolean
  noContentWhileLoading?: boolean
  children?: React.ReactNode
}
export default function LoadingContainer({ showLoader, noContentWhileLoading, children }: LoadingContainerParams) {
  if (!showLoader) return <>{children}</>
  return (
    <div className="dimmer active" style={{ minHeight: '50px' }}>
      <div className="loader" />
      <div className="dimmer-content">{!noContentWhileLoading && children}</div>
    </div>
  )
}
