import React from 'react'
import { render } from '@testing-library/react'
import LoadingContainer from '../index'

describe('Loading container', () => {
  test('Test of loading container', async () => {
    const testCode = (loading, noContent) => (
      <LoadingContainer showLoader={loading} noContentWhileLoading={noContent}>
        <h1>loading</h1>
      </LoadingContainer>
    )

    const renderResult = render(testCode(true, true))
    expect(renderResult.container).toMatchSnapshot()

    renderResult.rerender(testCode(true, false))
    expect(renderResult.container).toMatchSnapshot()

    renderResult.rerender(testCode(false, false))
    expect(renderResult.container).toMatchSnapshot()

    renderResult.rerender(testCode(false, true))
    expect(renderResult.container).toMatchSnapshot()
  })
})
