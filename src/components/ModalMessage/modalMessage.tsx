import React from 'react'
import Modal from '../Modal/modal'

export default function ModalMessage({ message, onDismiss, title }: { message: string; onDismiss: () => void; title?: string }) {
  if (message) {
    return (
      <Modal confirmLabel="OK" show onClose={onDismiss} onConfirm={onDismiss} title={title ?? ''}>
        {message}
      </Modal>
    )
  } else {
    return null
  }
}
