import MasterPage from './masterPage'
import React from 'react'

export const withMasterPageForPublic = (Component: any) => () => {
  return (
    <MasterPage isForPublic>
      <Component />
    </MasterPage>
  )
}

export const withPageContainer = (Component: any) => () => {
  return (
    <div className="my-3 my-md-4">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <Component />
          </div>
        </div>
      </div>
    </div>
  )
}
