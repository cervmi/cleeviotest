import React from 'react'
import Footer from '../appComponents/Footer'
import PublicHeader from '../appComponents/publicHeader'

type MasterPageType = {
  isForAuthenticatedUser?: boolean
  isForPublic?: boolean
  isForAdmin?: boolean
  isForCompanyTypeUser?: boolean
  isForPrivateTypeUser?: boolean
  children: React.ReactNode
}

export default function MasterPage({ children }: MasterPageType) {
  return (
    <>
      <div className="page">
        <div className="page-main">
          <PublicHeader />
          {children})
        </div>
        <Footer />
      </div>
    </>
  )
}
