import { ErrorAlert } from '../formComponents'
import { useTranslation } from 'react-i18next'
import React from 'react'

export default function MissingRights() {
  const { t } = useTranslation()
  return <ErrorAlert message={t('common:missingRights')} centeredText />
}
