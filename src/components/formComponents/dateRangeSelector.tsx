import React, { useState } from 'react'
import moment, { Moment } from 'moment-timezone'
import { IFormComponentBase } from './components/IFormComponentBaseInput'
import { FormComponentBaseInput } from './components/FormComponentBaseInput'
import { DateRangePicker } from 'react-dates'
import { StateRef } from 'valuelink'

import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'

interface IRangeDatePickerComp extends IFormComponentBase {
  onSelect?: Function
  minDate?: Date
  maxDate?: Date
  valueLinkFrom: StateRef<any>
  valueLinkTo: StateRef<any>
  id: string
  showAsBlock?: boolean
}

export default function DateRangeSelector({
  valueLinkFrom,
  valueLinkTo,
  minDate,
  maxDate,
  onSelect,
  showAsBlock = false,
  ...otherOptions
}: IRangeDatePickerComp) {
  const [focusedInput, setFocusedInput] = useState<any>(null)
  return (
    //it does not depend on value for valueLink, but only I want to transfer errors if any are set
    <FormComponentBaseInput valueLink={valueLinkFrom.error ? valueLinkFrom : valueLinkTo} {...otherOptions} defaultComponentStyle="form-control">
      {() => (
        <DateRangePicker
          startDate={valueLinkFrom.value ? moment(valueLinkFrom.value) : null}
          startDateId={`${otherOptions.id}DateFrom`}
          endDate={valueLinkTo.value ? moment(valueLinkTo.value) : null}
          endDateId={`${otherOptions.id}DateTo`}
          onDatesChange={({ startDate, endDate }: { startDate: Moment | null; endDate: Moment | null }) => {
            valueLinkFrom.set(startDate?.toDate() ?? undefined)
            valueLinkTo.set(endDate?.toDate() ?? undefined)
            if (onSelect) onSelect({ from: startDate?.startOf('day').toDate(), to: endDate?.endOf('day').toDate() })
          }}
          focusedInput={focusedInput}
          onFocusChange={focusedInput => setFocusedInput(focusedInput)}
          isOutsideRange={day => {
            return (minDate && day.isBefore(minDate, 'day')) || (maxDate && day.isAfter(maxDate, 'day'))
          }}
          small
          block={showAsBlock}
          required={otherOptions.required}
          disabled={otherOptions.disabled}
          showClearDates
        />
      )}
    </FormComponentBaseInput>
  )
}
