import React from 'react'
import { Input } from 'linked-controls'
import { useTouchDetectionComponent } from './components/touchDetectionComponent'
import Tooltip from './tooltip'

interface IToggleSwitchParam {
  text: any
  required?: boolean
  valueLink: any
  disabled?: boolean
  tooltip?: string
}

export default function ToggleSwitch({ text, required = false, valueLink, disabled = false, tooltip }: IToggleSwitchParam) {
  const { touched } = useTouchDetectionComponent(valueLink, disabled)
  return (
    <div className="form-group">
      <label className="custom-switch">
        <Input type="checkbox" className="custom-switch-input" $value={valueLink} disabled={disabled} />
        <span className="custom-switch-indicator" />
        <span className="custom-switch-description">
          {text}
          {required && <span className="form-required">*</span>}
          {tooltip && (
            <Tooltip placement="top" trigger="click" tooltip={tooltip} hideArrow={false}>
              <button className="icon">
                <i className="fe fe-help-circle ml-2" />
              </button>
            </Tooltip>
          )}
        </span>
      </label>
      {valueLink.error && touched && (
        <div className="invalid-feedback" style={{ display: 'block' }}>
          {valueLink.error}
        </div>
      )}
    </div>
  )
}
