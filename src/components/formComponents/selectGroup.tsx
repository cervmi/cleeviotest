import { Input } from 'linked-controls'
import React from 'react'
import { StateRef } from 'valuelink'
import { IReactSelectItem } from '../../services/types/ReactSelect'

interface SelectGroupParams {
  caption?: string
  options: IReactSelectItem[]
  valueLink: StateRef<string>
  handleChange?: (v: string) => void
  inline?: boolean
}

export default function SelectGroup({ caption, options, valueLink, handleChange, inline = false }: SelectGroupParams) {
  function handleClick(value: string): any {
    if (handleChange) return () => handleChange(value)
  }

  return (
    <div className={inline ? 'ml-5 mt-2' : 'form-group'}>
      {caption && <label className="form-label">{caption}</label>}
      <div className="selectgroup selectgroup-pills">
        {options.map(item => {
          return (
            <label className="selectgroup-item" key={item.value}>
              <Input type="radio" className="selectgroup-input" $value={valueLink} value={item.value} onClick={handleClick(item.value)} />
              <span className="selectgroup-button">{item.label}</span>
            </label>
          )
        })}
      </div>
    </div>
  )
}
