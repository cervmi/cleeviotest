import React from 'react'
import { Input } from 'linked-controls'
import { IStringObject } from '../../services/types/IStringObject'
import { IFormComponentBaseInput } from './components/IFormComponentBaseInput'
import { FormComponentBaseInput } from './components/FormComponentBaseInput'

interface IFormInputType extends IFormComponentBaseInput {
  isNumeric?: boolean
  isPassword?: boolean
  fieldSuffix?: React.ReactElement | string
  buttonSuffix?: { label: string; action: () => void }
  inputProps?: IStringObject
  name?: string
  autoFocus?: boolean
}

export default function FormInput({
  name,
  isNumeric,
  isPassword,
  valueLink,
  fieldSuffix,
  buttonSuffix,
  inputProps,
  autoFocus,
  ...otherOptions
}: IFormInputType) {
  return (
    <FormComponentBaseInput valueLink={valueLink} {...otherOptions}>
      {styleClass => (
        <div className="input-group">
          {isNumeric && (
            <Input
              $value={valueLink}
              type="number"
              name={name}
              className={'form-control ' + styleClass}
              disabled={otherOptions.disabled}
              {...inputProps}
              min="0"
              autoFocus={autoFocus}
            />
          )}
          {!isNumeric && (
            <Input
              $value={valueLink}
              name={name}
              type={isPassword ? 'password' : 'text'}
              className={'form-control ' + styleClass}
              disabled={otherOptions.disabled}
              {...inputProps}
              autoFocus={autoFocus}
            />
          )}

          {fieldSuffix && (
            <span className="input-group-append">
              <span className="input-group-text">{fieldSuffix}</span>
            </span>
          )}

          {buttonSuffix && (
            <span className="input-group-append">
              <button className="btn btn-primary" type="button" onClick={buttonSuffix.action}>
                {buttonSuffix.label}
              </button>
            </span>
          )}
        </div>
      )}
    </FormComponentBaseInput>
  )
}
