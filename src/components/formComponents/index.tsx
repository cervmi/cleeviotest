import React from 'react'
import { Alert, alertType, IAlertParams } from './alert'

export * from './alert'
export * from './datePickerComp'
export * from './formCard'
export * from './radioGroup'
export * from './recaptcha'
export * from './formInput'
export * from './htmlDataList'
export * from './toggleSwitch'
export * from './components/touchDetectionComponent'

export const SuccessAlert = (props: IAlertParams) => <Alert type={alertType.success} {...props} />
export const ErrorAlert = (props: IAlertParams) => <Alert type={alertType.danger} {...props} />
export const WarningAlert = (props: IAlertParams) => <Alert type={alertType.warning} {...props} />
export const InfoAlert = (props: IAlertParams) => <Alert type={alertType.info} {...props} />
