import React from 'react'
import LoadingContainer from '../LoadingContainer'

class FormCard extends React.Component {
  render() {
    const { title, titleData, isActive, isFilled, isCollapsed, editAction, showLoader, cardOptions } = this.props
    const collapsedClass = this.props.isCollapsed ? 'card-collapsed' : ''
    return (
      <>
        <div className={'card ' + collapsedClass}>
          {isActive && <div className="card-status card-status-left bg-yellow" />}
          {isFilled && !isActive && <div className="card-status card-status-left bg-green" />}
          {!isFilled && !isActive && <div className="card-status card-status-left bg-gray" />}
          <div className="card-header">
            <div className="card-title">
              <h1 className="card-title">{title}</h1>
            </div>
            {!isCollapsed && cardOptions && <div className="card-options">{cardOptions}</div>}
            {isCollapsed && titleData && (
              <div className="card-options ">
                <div className="mr-3">
                  {titleData.map(x => {
                    return (
                      <div key={x.key} className="card-titleText">
                        <strong>{x.key}:</strong> {x.value}
                      </div>
                    )
                  })}
                </div>

                <button className="btn btn-primary btn-sm" onClick={editAction}>
                  Edit
                </button>
              </div>
            )}
          </div>

          <LoadingContainer showLoader={showLoader}>
            <div className="card-body">{this.props.children}</div>
          </LoadingContainer>
        </div>
      </>
    )
  }
}

export default FormCard
