import React from 'react'
import { IStringObject } from '../../services/types/IStringObject'
import i18n from '../../services/appConfiguration/i18nSetup'

interface IHtmlDataListParams {
  label?: string
  data: IStringObject
  translate?: boolean
  omitKeys?: string[]
}

export function HtmlDataList({ label, data, omitKeys = [], translate = true }: IHtmlDataListParams) {
  if (!data || data.length <= 0) return null
  return (
    <section className="form-group">
      {label && (
        <header>
          <h2>{label}</h2>
        </header>
      )}
      <dl className="row">
        {
          //for map  [...data].map(([key, value]) => {
          Object.entries<any>(data)
            .filter(x => !omitKeys.includes(x[0]))
            .map(([key, value]) => {
              return (
                <React.Fragment key={key}>
                  <dt className="col-6">{translate ? i18n.t(key.indexOf(':') > 0 ? key : 'formFields:' + key) : key}</dt>
                  <dd className="col-6">{value}</dd>
                </React.Fragment>
              )
            })
        }
      </dl>
    </section>
  )
}
