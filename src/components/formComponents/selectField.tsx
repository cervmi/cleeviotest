import React from 'react'
import Select from 'react-select'
import { IReactSelectItem } from '../../services/types/ReactSelect'
import { useTranslation } from 'react-i18next'
import { FormComponentBaseInput } from './components/FormComponentBaseInput'
import { IFormComponentBaseInput } from './components/IFormComponentBaseInput'

interface ISelectFieldParams extends IFormComponentBaseInput {
  optionsData: IReactSelectItem<any>[]
  handleChange?: Function
  isLoading?: boolean
  placeholder?: string
  noOptionsMessage?: string
  customSelectedValue?: any
  styles?: any
  doNotUseDefaultSetMethod?: boolean
}

export default function SelectField({
  valueLink,
  optionsData,
  handleChange,
  isLoading,
  placeholder,
  noOptionsMessage,
  customSelectedValue,
  styles,
  doNotUseDefaultSetMethod,
  ...otherOptions
}: ISelectFieldParams) {
  const { t } = useTranslation()
  const selectedValue = customSelectedValue ?? optionsData.find(x => x.value === valueLink.value)

  if (!optionsData) optionsData = []
  return (
    <FormComponentBaseInput valueLink={valueLink} {...otherOptions}>
      {styleClass => (
        <Select
          // @ts-ignore
          id={valueLink?.key}
          options={optionsData}
          placeholder={placeholder ?? t('dropdownChooseItem')}
          noOptionsMessage={() => noOptionsMessage ?? t('noOptionsAvailable')}
          value={selectedValue ? selectedValue : null}
          onChange={(v: any) => {
            if (!doNotUseDefaultSetMethod && v && valueLink.value && v.value === valueLink.value) return
            if (!doNotUseDefaultSetMethod) valueLink.set(v ? v.value : null)
            if (handleChange) handleChange(v ? v.value : null)
          }}
          isClearable={!otherOptions.required}
          menuPlacement="auto"
          menuPosition="fixed"
          className={styleClass}
          classNamePrefix="reactSelect"
          isDisabled={otherOptions.disabled}
          isLoading={isLoading}
          styles={styles}
        />
      )}
    </FormComponentBaseInput>
  )
}
