import React, { useCallback } from 'react'
import { Input } from 'linked-controls'
import { IStringObject } from '../../services/types/IStringObject'
import { StateRef } from 'valuelink'

interface IRadioGroupParam {
  valueLink: StateRef<any>
  radioOptions: IRadioGroupOption[]
  label: string
  required?: boolean
  inline?: boolean
  setFn?: (x: IRadioGroupOption) => any
  onSelectionChange?: () => void
  groupName: string
}

export interface IRadioGroupOption extends IStringObject {
  label: string
  value: string
  disabled?: boolean
}

export default function RadioGroup({ onSelectionChange, groupName, label, required, radioOptions, valueLink, inline, setFn = x => x.value }: IRadioGroupParam) {
  const onSelectionChangeCB = useCallback(onSelectionChange ? onSelectionChange : () => undefined, [])
  valueLink.onChange(onSelectionChangeCB)
  return (
    <div className="form-group">
      <label className="form-label">
        {label}
        {required && <span className="form-required">*</span>}
      </label>
      <div className="custom-controls-stacked">
        {radioOptions.map(ro => {
          const valueOfValueLink = valueLink.value && valueLink.value.value === ro.value ? setFn(valueLink.value) : setFn(ro)
          return (
            <label className={'custom-control custom-radio' + (inline ? ' custom-control-inline' : '')} key={ro.value}>
              <Input type="radio" $value={valueLink} value={valueOfValueLink} className="custom-control-input" disabled={!!ro.disabled} name={groupName} />
              <span className="custom-control-label">{ro.label}</span>
            </label>
          )
        })}
      </div>
    </div>
  )
}
