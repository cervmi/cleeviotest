import React, { useState } from 'react'
import moment, { Moment } from 'moment'
import { IFormComponentBaseInput } from './components/IFormComponentBaseInput'
import { FormComponentBaseInput } from './components/FormComponentBaseInput'
import { SingleDatePicker } from 'react-dates'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import './../../css/react_dates_overrides.scss'
import { useTranslation } from 'react-i18next'

interface IDatePickerComp extends IFormComponentBaseInput {
  onSelect?: Function
  minDate?: Date
  maxDate?: Date
  id: string
  showAsBlock?: boolean
  disableWeekends?: boolean
}

export default function DatePickerComp({ valueLink, disableWeekends, minDate, maxDate, onSelect, showAsBlock = false, ...otherOptions }: IDatePickerComp) {
  const [focused, setFocused] = useState<boolean | null>(null)
  const { t } = useTranslation()

  return (
    <FormComponentBaseInput valueLink={valueLink} {...otherOptions} defaultComponentStyle="form-control">
      {() => (
        <SingleDatePicker
          id={otherOptions.id ?? 'datepicker'}
          date={valueLink.value ? moment(valueLink.value) : null}
          onDateChange={(date: Moment | null) => {
            valueLink.set(date?.toDate())
            if (onSelect) onSelect(date?.toDate())
          }}
          placeholder={t('selectDatePlaceholder')}
          focused={focused}
          onFocusChange={({ focused }) => setFocused(focused)}
          isOutsideRange={day => (minDate && day.isBefore(minDate, 'day')) || (maxDate && day.isAfter(maxDate, 'day')) || (disableWeekends && isWeekend(day))}
          showClearDate={!otherOptions.required}
          disabled={otherOptions.disabled}
          small
          block={showAsBlock}
        />
      )}
    </FormComponentBaseInput>
  )
}

function isWeekend(date: Moment) {
  return date.day() === 0 || date.day() === 6
}
