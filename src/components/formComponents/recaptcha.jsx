import React from 'react'
import ReCAPTCHA from 'react-google-recaptcha'
import { useTranslation } from 'react-i18next'

const TEST_SITE_KEY = '6Lf_644UAAAAAF-CxZz49KMyLDWo-SNiRevfR7u2'

export default function Recaptcha({ valueLink }) {
  const { i18n } = useTranslation()
  window.recaptchaOptions = {
    lang: i18n.language,
    useRecaptchaNet: true
  }
  return <ReCAPTCHA sitekey={TEST_SITE_KEY} onChange={value => valueLink.set(value)} hl={i18n.language} />
}
