import React from 'react'

export enum alertType {
  danger = 'danger',
  warning = 'warning',
  info = 'info',
  success = 'success',
  secondary = 'secondary',
  primary = 'primary'
}

export interface IAlertType extends IAlertParams {
  type: alertType
}

export interface IAlertParams {
  message?: React.ReactElement | string
  centeredText?: boolean
  isDismissible?: boolean
  onDismiss?: any
  children?: React.ReactNode
}

export function Alert({ type, message, centeredText, isDismissible, onDismiss, children }: IAlertType) {
  if (!type || (!message && !children)) return null

  let className: string = 'card-alert alert mb-0 alert-' + type
  if (centeredText) className = className + ' text-center'
  if (isDismissible) className = className + ' alert-dismissible'

  return (
    <div className={className + ' mb-3'} role="alert">
      {!!isDismissible && onDismiss && <button type="button" className="close" onClick={onDismiss} />}
      {message}
      {children}
    </div>
  )
}
