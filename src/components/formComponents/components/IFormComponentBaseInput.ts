import { StateRef } from 'valuelink'
import React from 'react'

export interface IFormComponentBaseInput extends IFormComponentBase {
  valueLink: StateRef<any>
}

export interface IFormComponentBase {
  label?: string
  required?: boolean
  disabled?: boolean
  helperText?: React.ReactElement | string
  inline?: boolean
  tooltip?: string
  hideValidationError?: boolean
  resetInput?: boolean
  defaultComponentStyle?: string
  children?: (styleClass: string) => React.ReactNode
}
