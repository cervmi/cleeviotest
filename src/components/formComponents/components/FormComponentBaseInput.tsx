import { IFormComponentBaseInput } from './IFormComponentBaseInput'
import { useTouchDetectionComponent } from './touchDetectionComponent'
import Tooltip from '../tooltip'
import React from 'react'

export function FormComponentBaseInput({
  valueLink,
  disabled,
  helperText,
  inline,
  label,
  required,
  hideValidationError,
  tooltip,
  resetInput,
  defaultComponentStyle = '',
  children
}: IFormComponentBaseInput) {
  const { styleClass, touched, reset } = useTouchDetectionComponent(valueLink, !!disabled, defaultComponentStyle)
  if (touched && resetInput) reset()
  return (
    <div className={inline ? '' : 'form-group'}>
      {label && (
        // @ts-ignore
        <label className="form-label" style={{ textAlign: 'left' }} htmlFor={valueLink.key}>
          {label}
          {required && <span className="form-required">*</span>}
          {helperText && <div className="float-right small ml-1">{helperText}</div>}
          {tooltip && (
            <div className="float-right">
              <Tooltip placement="top" trigger="click" tooltip={tooltip} hideArrow={false}>
                <button className="icon">
                  <i className="fe fe-help-circle" />
                </button>
              </Tooltip>
            </div>
          )}
        </label>
      )}
      {children && children(styleClass)}
      {valueLink.error && touched && !hideValidationError && (
        <div className="invalid-feedback" style={{ display: 'block' }}>
          {valueLink.error}
        </div>
      )}
    </div>
  )
}
