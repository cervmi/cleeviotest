import { StateRef } from 'valuelink'
import { useEffect, useState } from 'react'
import classNames from 'classnames'

export function useTouchDetectionComponent(
  valueLink: StateRef<any>,
  disabled: boolean,
  defaultStyle = ''
): { styleClass: string; touched: boolean; reset: () => void } {
  const [touched, setTouched] = useState<boolean>(false)

  useEffect(() => {
    if (valueLink.value) setTouched(true)
  }, [valueLink.value])

  let requiredValidationStyle = ''
  if (valueLink.error && !disabled) {
    requiredValidationStyle = touched ? 'is-invalid' : 'is-required'
  }

  return { styleClass: classNames(defaultStyle, requiredValidationStyle), touched, reset }

  function reset() {
    valueLink.set('')
    setTouched(false)
  }
}
