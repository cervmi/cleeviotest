import React from 'react'
import { useTranslation } from 'react-i18next'
import { MenuItem } from '../MainMenu/menuItem'
import { MainMenuContainer } from '../MainMenu/mainMenuContainer'

export default function HorizontalMenu({ expandHorizontalMenu }: { expandHorizontalMenu: boolean }) {
  const { t } = useTranslation()
  return (
    <>
      <MainMenuContainer expandHorizontalMenu={expandHorizontalMenu}>
        <>
          <MenuItem label={t('menu.home')} link="/" iconStyle="fe-home" />
          <MenuItem label={t('general.register')} link="/register" iconStyle="fe-plus-square" />
          <MenuItem label={t('general.login')} link="/login" iconStyle="fe-log-in" displayOnMobileOnly />
        </>
      </MainMenuContainer>
    </>
  )
}
