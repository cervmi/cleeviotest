import React, { useCallback, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Avatar } from './avatar'

export default function AvatarWithMenuComponent() {
  const { t } = useTranslation()
  const history = useHistory()
  const [menuVisible, setMenuVisible] = useState(false)

  const hideMenuWhenClickedInDocumentCB = useCallback(hideMenuWhenClickedInDocument, [])
  useEffect(() => {
    return () => {
      document.removeEventListener('click', hideMenuWhenClickedInDocumentCB)
    }
  }, [hideMenuWhenClickedInDocumentCB])
  const avatarName = 'Logged user'

  return (
    <div className={'dropdown ' + (menuVisible ? 'show' : '')}>
      <button className="nav-link pr-0 leading-none transparentButton" onClick={toggleMenu}>
        <Avatar name={avatarName} large={false} isNotMe={false} />
        <span className="ml-2 d-none d-lg-block">
          <span className="text-default">Logged user</span>
          <small className="text-muted d-block mt-1">My name</small>
        </span>
      </button>
      {menuVisible && (
        <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow show zIndex100">
          <div role="button" tabIndex={0} className="dropdown-item cursorPointer" onMouseDown={() => goTo('/')}>
            <i className="dropdown-icon fe fe-user" /> {t('menu.myProfile')}
          </div>
          <div className="dropdown-divider" />
          <div role="button" tabIndex={0} className="dropdown-item cursorPointer" onMouseDown={logOut}>
            <i className="dropdown-icon fe fe-log-out" /> {t('general.logout')}
          </div>
        </div>
      )}
    </div>
  )

  function toggleMenu(event: any) {
    event.preventDefault()
    if (menuVisible) hideMenu()
    else showMenu()
  }

  function hideMenu() {
    setMenuVisible(false)
    document.removeEventListener('click', hideMenuWhenClickedInDocument)
  }

  function showMenu() {
    setMenuVisible(true)
    document.addEventListener('click', hideMenuWhenClickedInDocument)
  }

  function logOut() {
    hideMenu()
    history.push('/')
  }

  function goTo(to: string) {
    hideMenu()
    history.push(to)
  }

  function hideMenuWhenClickedInDocument(event: Event) {
    event.preventDefault()
    hideMenu()
  }
}
