import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Logo } from './logo'
import { useTranslation } from 'react-i18next'
import LoginModal from '../../scenesPublic/Login/loginModal'
import LanguageSelector from './languageSelector'
import HorizontalMenu from './horizontalMenu'

export default function PublicHeader() {
  const [state, setState] = useState({ showLogin: false, expandHorizontalMenu: false })
  const { t } = useTranslation()

  return (
    <>
      <div className="header py-2">
        <div className="container">
          <div className="d-flex">
            <Logo href="/" />
            <div className="d-flex order-lg-2 ml-auto">
              <LanguageSelector />
              {window.location.pathname !== '/register' && (
                <div className="nav-item d-none d-sm-flex">
                  <Link to="/register" className="btn btn-sm btn-outline-primary">
                    {t('general.register')}
                  </Link>
                </div>
              )}
              <div className="nav-item d-none d-sm-flex">
                <button className="btn btn-sm btn-outline-primary" onClick={e => showLogin(e)}>
                  {t('general.login')}
                </button>
              </div>
            </div>
            <button className="header-toggler d-lg-none ml-3 ml-lg-0" onClick={() => setState({ ...state, expandHorizontalMenu: !state.expandHorizontalMenu })}>
              <span className="header-toggler-icon" />
            </button>
          </div>
        </div>
      </div>
      <HorizontalMenu expandHorizontalMenu={state.expandHorizontalMenu} />
      <LoginModal show={state.showLogin} onClose={hideLogin} />
    </>
  )

  function showLogin(e: any) {
    e.preventDefault()
    setState({ ...state, showLogin: true })
  }

  function hideLogin() {
    setState({ ...state, showLogin: false })
  }
}
