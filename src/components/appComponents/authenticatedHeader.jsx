import React from 'react'
import AvatarWithMenuComponent from './avatarWithMenuComponent'
import { Logo } from './logo'
import LanguageSelector from './languageSelector'
import HorizontalMenu from './horizontalMenu'

export default class AuthenticatedHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state = { expandHorizontalMenu: false }
  }

  render() {
    return (
      <>
        <div className="header py-2">
          <div className="container">
            <div className="d-flex">
              <Logo href="/dashboard" />
              <div className="d-flex order-lg-2 ml-auto">
                <LanguageSelector />
                <AvatarWithMenuComponent />
                <a href="# " onClick={() => this.showHorizontalMenu()} role="button" className="header-toggler d-lg-none ml-3 ml-lg-0">
                  <span className="header-toggler-icon" />
                </a>
              </div>
            </div>
          </div>
        </div>
        <HorizontalMenu expandHorizontalMenu={this.state.expandHorizontalMenu} />
      </>
    )
  }

  showHorizontalMenu() {
    this.setState(prevState => ({
      expandHorizontalMenu: !prevState.expandHorizontalMenu
    }))
  }
}
