import React from 'react'
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import UserAvatar from 'react-user-avatar'

//const avatarBgColors = ['#FF0000', '#A60000', '#FF7373', '#FF7400', '#A64B00', '#FFB273', '#CD0074', '#85004B', '#E667AF', '#00CC00', '#008500', '#67E667']
const avatarBgColors = ['#FF0000', '#FF7373', '#FF7400', '#FFB273', '#CD0074', '#E667AF', '#00CC00', '#67E667']

export function Avatar({ name, large, isNotMe }: { name: string; large: boolean; isNotMe: boolean }) {
  return <UserAvatar size={large ? 90 : 48} name={'test user'} colors={avatarBgColors} className={large ? 'card-profile-img ml-auto mr-auto' : ''} />
}
