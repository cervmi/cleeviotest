import React from 'react'
import { useTranslation } from 'react-i18next'

export default function LanguageSelector() {
  const { i18n } = useTranslation()
  return (
    <div className="nav-item d-md-flex languageFlag">
      <div role="button" onClick={changeToCz} onKeyDown={changeToCz} tabIndex={0}>
        <i className={'flag flag-cz ' + (i18n.language === 'cs' ? 'current' : '')} />
      </div>
      <div role="button" onClick={changeToEn} onKeyDown={changeToEn} tabIndex={0}>
        <i className={'flag flag-us ' + (i18n.language === 'en' ? 'current' : '')} />
      </div>
    </div>
  )

  function changeToCz() {
    changeLanguage('cs')
  }

  function changeToEn() {
    changeLanguage('en')
  }

  function changeLanguage(countryIso: string) {
    i18n.changeLanguage(countryIso)
  }
}
