import React from 'react'
import { useTranslation } from 'react-i18next'
import { name, version } from '../../../package.json'
import moment from 'moment'

export default function Footer() {
  const { t } = useTranslation()
  return (
    <>
      <div className="footerPlaceholder" />
      <footer className="footer">
        <div className="container">
          <div className="row align-items-center flex-row-reverse">
            <div className="col-auto ml-lg-auto">
              <ul className="list-inline list-inline-dots mb-0">
                <li className="list-inline-item">
                  {name} v.{version}
                </li>
              </ul>
            </div>
            <div className="col-md-3 d-none d-lg-block">{`${t('general.allRightsReserved')} ${moment().year()}`}</div>
          </div>
        </div>
      </footer>
    </>
  )
}
