import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLink } from 'valuelink'
import { Input } from 'linked-controls'
import { useHistory } from 'react-router-dom'
import { httpBuildQueryString } from '../../services/functions/httpBuildQuery'

export default function SearchForPacketInput() {
  const searchFor = { code: '' }
  const searchForLink = useLink(searchFor).pick('code').code
  const history = useHistory()
  const { t } = useTranslation()
  return (
    <form className="input-group" onSubmit={searchForPacket}>
      <Input $value={searchForLink} type="search" className="form-control header-search" placeholder={t('menu.packetCodeForSearch')} />
      <span className="input-group-append">
        <button className="btn btn-primary" type="submit">
          <i className="fe fe-search" />
        </button>
      </span>
    </form>
  )

  function searchForPacket(e: any) {
    e.preventDefault()
    if (searchForLink.value) history.push('/packet-tracking' + httpBuildQueryString({ code: searchForLink.value }))
  }
}
