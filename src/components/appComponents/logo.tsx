import React from 'react'
import '../../css/general.css'
import { Link } from 'react-router-dom'

export function Logo({ href }: { href: string }) {
  return (
    <Link className="header-brand" to={href}>
      <h3 className="logo">
        <img src="/images/logo.png" className="header-brand-img mt-2" alt="logo" />
        <span className="long" />
        <span className="short" />
      </h3>
    </Link>
  )
}
