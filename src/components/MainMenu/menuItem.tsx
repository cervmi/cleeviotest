import React, { useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import classNames from 'classnames'
import { IStringObject } from '../../services/types/IStringObject'

type MenuItemType = {
  link: string
  label: string
  displayOnMobileOnly?: boolean
  iconStyle: string
  children?: React.ReactNode
}
export function MenuItem({ link, label, displayOnMobileOnly = false, iconStyle, children }: MenuItemType) {
  const location = useLocation<IStringObject>()
  const history = useHistory()
  const [showSubmenu, setShowSubmenu] = useState(false)

  const isActive = location.pathname === link
  const displayOnMobileOnlyStyle = displayOnMobileOnly === true ? 'd-block d-sm-none' : ''
  const navItemClassNames = classNames('nav-item', displayOnMobileOnlyStyle, children ? 'dropdown' : '', showSubmenu ? 'show' : '')
  return (
    <li className={navItemClassNames}>
      <a
        className={classNames('nav-link', isActive || showSubmenu ? 'active' : '')}
        onClick={navigate}
        href={link}
        aria-current={isActive ? 'page' : undefined}
        aria-expanded={showSubmenu ? true : undefined}
      >
        <i className={'fe ' + iconStyle} /> {label}
      </a>
      {children && <div className={classNames('dropdown-menu', 'dropdown-menu-arrow', showSubmenu ? 'show' : '')}>{children}</div>}
    </li>
  )

  function navigate(e: any) {
    e.preventDefault()
    if (children) setShowSubmenu(x => !x)
    else if (isActive) {
      history.replace('/reload')
      setTimeout(() => {
        history.replace(link)
      })
    } else {
      history.push(link)
    }
  }
}
