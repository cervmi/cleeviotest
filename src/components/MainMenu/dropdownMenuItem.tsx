import React from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import classNames from 'classnames'
import { IStringObject } from '../../services/types/IStringObject'

export function DropdownMenuItem({ link, label, iconStyle }: { link: string; label: string; iconStyle: string }) {
  const location = useLocation<IStringObject>()
  const history = useHistory()
  const isActive = location.pathname === link
  return (
    <a className={classNames('dropdown-item', isActive ? 'active' : '')} onClick={navigate} href={link} aria-current={isActive ? 'page' : undefined}>
      <i className={'fe ' + iconStyle} /> {label}
    </a>
  )

  function navigate(e: any) {
    e.preventDefault()
    if (isActive) {
      history.replace('/reload')
      setTimeout(() => {
        history.replace(link)
      })
    } else {
      history.push(link)
    }
  }
}
