import React from 'react'

export function MainMenuContainer({ expandHorizontalMenu, children }: { expandHorizontalMenu?: boolean; children: React.ReactNode }) {
  const classAdd = expandHorizontalMenu ? ' show' : ''
  return (
    <div className={'header collapse d-lg-flex p-0' + classAdd} id="headerMenuCollapse">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-2 ml-auto">
            <div className="input-icon my-3 my-lg-0"></div>
          </div>
          <div className="col-lg order-lg-first">
            <ul className="nav nav-tabs border-0 flex-column flex-lg-row">{children}</ul>
          </div>
        </div>
      </div>
    </div>
  )
}
