import { isEmail, isRequired } from 'linked-controls'
import i18n from '../appConfiguration/i18nSetup'
import moment from 'moment/moment'
import { FieldValidation } from '../requiredFields/fieldType'
import { czNamesGender } from '../../locales/czNamesGender'
import { StateRef } from 'valuelink'

export function isStrongPassword(password: string): boolean {
  const regex = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,64})/g
  const mediumRegex = new RegExp(regex)

  return mediumRegex.test(password)
}

export function isPhoneNumber(phoneNumber: string): boolean {
  const regex = /^[+]{0,1}[(]{0,1}[0-9]{1,4}[)]{0,1}[\s0-9]{6,20}$/g
  const fullPhoneRegEx = new RegExp(regex)
  return fullPhoneRegEx.test(phoneNumber)
}

export function addRequiredCheckToValueLink(valueLink: StateRef<any>, formFieldsName: string, translationPrefixWithDot: string | undefined = undefined) {
  valueLink.check(
    (x: any) => isRequired(x),
    i18n.t('validationError.cannotBeEmptyError', {
      context: czNamesGender[formFieldsName],
      item: (translationPrefixWithDot ?? 'formFields:') + formFieldsName
    })
  )
}

export function addValidationCheck(vl: any, fieldCheck: FieldValidation, checkValue?: any) {
  switch (fieldCheck) {
    case FieldValidation.isPositive:
      vl.check(
        (x: any) => !isRequired(x) || x >= 0,
        i18n.t('validationError.onlyPositive', {
          item: 'formFields:' + vl.key,
          context: czNamesGender[vl.key]
        })
      )
      break
    case FieldValidation.isNumber:
      vl.check((x: any) => !isRequired(x) || !isNaN(x), i18n.t('validationError.hasToBeANumberError', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.isInteger:
      vl.check((x: any) => !isRequired(x) || Math.trunc(x) === Number(x), i18n.t('validationError.onlyInteger', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.lessThen:
      vl.check((x: any) => !isRequired(x) || x < Number(checkValue), i18n.t('validationError.outOfRange', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.moreThen:
      vl.check((x: any) => !isRequired(x) || x > Number(checkValue), i18n.t('validationError.outOfRange', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.lessThenOrEqual:
      vl.check((x: any) => !isRequired(x) || x <= Number(checkValue), i18n.t('validationError.outOfRange', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.moreThenOrEqual:
      vl.check((x: any) => !isRequired(x) || x >= Number(checkValue), i18n.t('validationError.outOfRange', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.isEmail:
      vl.check((x: any) => !isRequired(x) || isEmail(x), i18n.t('validationError.wrongFormat', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.isPhone:
      vl.check((x: any) => !isRequired(x) || isPhoneNumber(x), i18n.t('validationError.wrongFormat', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.maxLength:
      vl.check((x: any) => !isRequired(x) || x.length <= Number(checkValue), i18n.t('validationError.maxLengthError', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.minLength:
      vl.check((x: any) => !isRequired(x) || x.length >= Number(checkValue), i18n.t('validationError.minLengthError', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.isDate:
      vl.check((x: any) => !isRequired(x) || moment(x).isValid(), i18n.t('validationError.wrongFormat', { item: 'formFields:' + vl.key }))
      break
    case FieldValidation.minDate:
      vl.check(
        (x: any) => !isRequired(x) || moment(x) > moment().add(Number(checkValue - 1), 'days'),
        i18n.t('validationError.outOfRange', { item: 'formFields:' + vl.key })
      )
      break
    case FieldValidation.maxDate:
      vl.check(
        (x: any) => !isRequired(x) || moment(x) < moment().add(Number(checkValue + 1), 'days'),
        i18n.t('validationError.outOfRange', { item: 'formFields:' + vl.key })
      )
      break
    case FieldValidation.isTrue:
      vl.check((x: any) => !!x, i18n.t('validationError.hasToBeTrue', { item: 'formFields:' + vl.key }))
      break
  }
}

export function addFieldErrorFromMutationResult(vl: StateRef<any>, fieldErrorString: string, errorValue: string) {
  vl.check(x => x !== errorValue, i18n.t('mutationResult:errorList.' + fieldErrorString))
}
