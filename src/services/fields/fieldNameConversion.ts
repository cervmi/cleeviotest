//Converts UPPER_CASE GraphQL column name to camelCase
export function fieldNameConversion(input: string): string {
  let output = ''
  let toUpperCase = false

  for (let i = 0; i < input.length; i++) {
    if (input[i] === '_') {
      toUpperCase = true
      continue
    }
    if (toUpperCase) {
      toUpperCase = false
      output = output + input[i].toUpperCase()
    } else {
      output = output + input[i].toLowerCase()
    }
  }
  return output
}

//Converts camelCase to GraphQl column name->UPPER_CASE
export function fieldNameConversionReverse(input: string): string {
  let output = ''
  for (let i = 0; i < input.length; i++) {
    if (input[i] === input[i].toUpperCase()) output = output + '_'
    output = output + input[i].toUpperCase()
  }
  return output
}
