import { fieldNameConversion, fieldNameConversionReverse } from '../fieldNameConversion'

describe('Fields tests', () => {
  test('Field name conversion', async () => {
    expect(fieldNameConversion('CUSTOMS_CODE')).toBe('customsCode')
    expect(fieldNameConversion('CUSTOMS_INVOICE_ISSUE_DATE')).toBe('customsInvoiceIssueDate')
    expect(fieldNameConversion('CUSTOMS')).toBe('customs')
  })

  test('Field name reverse conversion', async () => {
    expect(fieldNameConversionReverse('customsCode')).toBe('CUSTOMS_CODE')
    expect(fieldNameConversionReverse('customsInvoiceIssueDate')).toBe('CUSTOMS_INVOICE_ISSUE_DATE')
    expect(fieldNameConversionReverse('customs')).toBe('CUSTOMS')
  })
})
