import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import localeAppCs from '../../locales/cs/app.json'
import localeAppEn from '../../locales/en/app.json'
import localeCommonCs from '../../locales/cs/common.json'
import localeCommonEn from '../../locales/en/common.json'
import localeFormFieldsCs from '../../locales/cs/formFields.json'
import localeFormFieldsEn from '../../locales/en/formFields.json'
import backend from 'i18next-xhr-backend'

// import LanguageDetector from 'i18next-browser-languagedetector';

function saveMissingTranslation(lng, ns, key, fallbackValue) {
  console.log(`Missing translation key: ${lng}-${ns}:${key} - fallback to ${fallbackValue}`)
}

const translationResources = {
  en: {
    app: localeAppEn,
    common: localeCommonEn,
    formFields: localeFormFieldsEn
  },
  cs: {
    app: localeAppCs,
    common: localeCommonCs,
    formFields: localeFormFieldsCs
  }
}

// for all options read: https://www.i18next.com/overview/configuration-options
i18n
  .use(backend)
  .use(initReactI18next)
  .init({
    namespaces: ['app', 'common', 'mutationResult'],
    resources: translationResources,
    lng: 'en',
    defaultNS: 'app',
    fallbackNS: 'common',
    fallbackLng: 'en',
    debug: false,
    saveMissing: true,
    appendNamespaceToMissingKey: true,
    missingKeyHandler: saveMissingTranslation,
    interpolation: {
      escapeValue: false // not needed for react as it escapes by default
    }
  })

export default i18n
