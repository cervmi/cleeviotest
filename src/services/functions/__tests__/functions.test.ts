import { mergeArrayOfObjects } from '../mergeArrayObjects'
import { setEmptyIfNullValue } from '../setEmptyIfNullValue'
import { convertDateTimeToUtc, convertDateToUtc, setCurrentTimeZone } from '../timezones'
import moment from 'moment'
import { FieldType } from '../../requiredFields/fieldType'
import { getParamFromUrl } from '../getParamFromUrl'
import { IStringObject } from '../../types/IStringObject'
import { IFieldDefinition, IFieldDefinitionExtension } from '../../dynamicFormFields/FieldDefinitionTypes'

describe('General functions test', () => {
  test('Array merging function', async () => {
    const mainArray: IFieldDefinition[] = [
      { field: 'item1', type: FieldType.boolean },
      { field: 'item2', type: FieldType.customsDuty }
    ]
    const secondArray: IFieldDefinitionExtension[] = [
      { field: 'item1', order: 1 },
      { field: 'item2', order: 2 }
    ]

    expect(mergeArrayOfObjects<IFieldDefinition, IFieldDefinitionExtension>(mainArray, secondArray, 'field')).toMatchObject([
      { field: 'item1', type: FieldType.boolean, order: 1 },
      { field: 'item2', type: FieldType.customsDuty, order: 2 }
    ])
    expect(mergeArrayOfObjects<IFieldDefinition, IFieldDefinitionExtension>(mainArray, secondArray, '')).toMatchObject([])

    expect(mainArray).toMatchObject([
      { field: 'item1', type: FieldType.boolean, order: 1 },
      { field: 'item2', type: FieldType.customsDuty, order: 2 }
    ])
  })

  test('Set empty if null value', async () => {
    expect(setEmptyIfNullValue({ test: null }, ['test'])).toMatchObject({ test: '' })
    expect(setEmptyIfNullValue({ test: 'value' }, ['test'])).toMatchObject({ test: 'value' })
    expect(setEmptyIfNullValue({}, ['test'])).toMatchObject({})
  })

  test('Timezone conversion tests', async () => {
    setCurrentTimeZone('America/New_York')
    expect(moment('2019-11-12T00:00:00Z').format('LLL')).toMatchInlineSnapshot('"November 11, 2019 7:00 PM"')
    expect(convertDateToUtc('2019-11-12')).toMatchInlineSnapshot('"2019-11-12T05:00:00.000Z"')
    expect(convertDateTimeToUtc(moment('2019-11-12'))).toMatchInlineSnapshot('"2019-11-12T05:00:00.000Z"')
  })

  test('HTTP get param from URL', async () => {
    window.history.pushState(
      {},
      'Test Title',
      '/testURL?item1=Lorem%20ipsum%20!%40%24%23%26%25%5E*(()%3C.%3C%3F%C5%A1%C4%9B%C4%8Dq%C4%8D%2F%5C%C5%A1%C5%99%C5%99%C5%BE&item2=basic%20test'
    )

    expect(getParamFromUrl('item2')).toMatchInlineSnapshot('"basic test"')
    expect(getParamFromUrl('item1')).toMatchInlineSnapshot('"Lorem ipsum !@$#&%^*(()<.<?šěčqč/\\\\šřřž"')
  })

  test('Set empty if null', async () => {
    const userData: IStringObject = { abc: 1233, def: 'dfadsf ds', ghi: false, zzz: null }
    setEmptyIfNullValue(userData, ['abc', 'def', 'ghi', 'zzz'])
    expect(userData).toMatchSnapshot()
  })
})
