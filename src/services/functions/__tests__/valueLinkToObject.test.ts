import valueLinkToObject from '../valueLinkToObject'
import Link, { RefsHash } from 'valuelink'

describe('General tests', () => {
  it('RefsHash is converted to object with values', () => {
    const testField: RefsHash = {
      item1: Link.value(1, () => undefined),
      item2: Link.value(2, () => undefined),
      item3: Link.value(3, () => undefined)
    }
    expect(valueLinkToObject(testField)).toMatchObject({ item1: 1, item2: 2, item3: 3 })
  })
})
