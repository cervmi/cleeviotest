import React from 'react'
import { act, fireEvent, render } from '@testing-library/react'
import { enterListener } from '../enterListener'

describe('General functions test2', () => {
  test('Enter listener test', async () => {
    const testFn = jest.fn(() => undefined)

    const renderResult = render(<input onKeyDown={enterListener(testFn)} data-testid={'testInput'} />)

    const testInput = renderResult.getByTestId('testInput')
    act(() => {
      fireEvent.keyDown(testInput, 'asf')
    })

    expect(testFn).toHaveBeenCalledTimes(0)
    act(() => {
      fireEvent.keyDown(testInput, { key: 'Enter', code: 13 })
    })

    expect(testFn).toHaveBeenCalledTimes(1)
  })
})
