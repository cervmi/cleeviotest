import { IReactSelectItem } from '../../types/ReactSelect'
import distinctArrayForReactSelect from '../distinctArrayForReactSelect'

describe('General tests', () => {
  it('Array of react select items gets deduplicated', () => {
    const testField: IReactSelectItem[] = [
      { label: 'A', value: 1 },
      { label: 'A', value: 1 },
      { label: 'A', value: 1 },
      { label: 'B', value: 2 }
    ]

    const result = distinctArrayForReactSelect(testField)
    expect(result.length).toBe(2)
    expect(result).toMatchObject([
      { label: 'A', value: 1 },
      { label: 'B', value: 2 }
    ])
  })
})
