import { httpBuildQueryString } from '../httpBuildQuery'

describe('General tests', () => {
  it('Query is build from object', () => {
    const testObject = {
      item1: 'Lorem ipsum !@$#&%^*(()<.<?šěčqč/\\šřřž',
      item2: 'basic test',
      item3: undefined,
      item4: ''
    }
    expect(httpBuildQueryString(testObject)).toMatchInlineSnapshot(
      '"?item1=Lorem%20ipsum%20!%40%24%23%26%25%5E*(()%3C.%3C%3F%C5%A1%C4%9B%C4%8Dq%C4%8D%2F%5C%C5%A1%C5%99%C5%99%C5%BE&item2=basic%20test"'
    )
  })
})
