import { IReactSelectItem } from '../../types/ReactSelect'
import { sortArrayBy, sortArrayByNumeric } from '../sortArrayBy'

describe('General functions test', () => {
  test('Sort arrray by', async () => {
    const testArray: IReactSelectItem[] = [
      { value: 'item1', label: 'abc' },
      { value: 'item2', label: 'ADDD' },
      { value: 'item3', label: 'aattt' },
      { value: 'item4', label: 'dsdsdfsdf' }
    ]
    const testArray2: any = [
      { value: 'item1', label: 5 },
      { value: 'item2', label: 3 },
      { value: 'item3', label: 1 },
      { value: 'item55', label: 5 },
      { value: 'item4', label: 6 }
    ]

    expect(sortArrayBy(testArray, x => x.label)).toMatchObject([
      {
        label: 'aattt',
        value: 'item3'
      },
      {
        label: 'abc',
        value: 'item1'
      },
      {
        label: 'ADDD',
        value: 'item2'
      },
      {
        label: 'dsdsdfsdf',
        value: 'item4'
      }
    ])
    expect(sortArrayByNumeric(testArray2, x => x.label)).toMatchObject([
      {
        label: 1,
        value: 'item3'
      },
      {
        label: 3,
        value: 'item2'
      },
      {
        label: 5,
        value: 'item1'
      },
      {
        label: 5,
        value: 'item55'
      },
      {
        label: 6,
        value: 'item4'
      }
    ])
  })
})
