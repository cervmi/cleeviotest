import moment from 'moment'
import { Moment } from 'moment-timezone/moment-timezone'
require('moment-timezone')

export function setCurrentTimeZone(timezone: string) {
  moment.tz.setDefault(timezone)
}

export function convertDateToUtc(date: string | Date | Moment) {
  return convertDateTimeToUtc(moment(date).startOf('day'))
}

export function convertDateTimeToUtc(dateTime: string | Date | Moment) {
  return moment(dateTime).tz('UTC').toISOString()
}
