import { IStringObject } from '../types/IStringObject'

export function httpBuildQueryString(param: IStringObject) {
  return (
    '?' +
    Object.keys(param)
      .map(key => {
        if (param[key] !== false && !param[key]) return ''
        return key + '=' + encodeURIComponent(param[key])
      })
      .filter(x => !!x)
      .join('&')
  )
}
