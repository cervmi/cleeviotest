import { IReactSelectItem } from '../types/ReactSelect'

export default function distinctArrayForReactSelect(array: IReactSelectItem[]): IReactSelectItem[] {
  const result = []
  const map = new Map()
  for (const item of array) {
    if (!map.has(item.value)) {
      map.set(item.value, true) // set any value to Map
      result.push(item)
    }
  }
  return result
}
