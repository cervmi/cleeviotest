import { RefsHash } from 'valuelink'
import { IStringObject } from '../types/IStringObject'

export default function valueLinkToObject(valueLinks: RefsHash) {
  const returnObject: IStringObject = {}
  for (const item of Object.keys(valueLinks)) {
    returnObject[item] = valueLinks[item].value
  }
  return returnObject
}
