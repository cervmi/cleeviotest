import { IStringObject } from '../types/IStringObject'

export function setEmptyIfNullValue(data: IStringObject, itemNames: string[]) {
  for (const item of itemNames) {
    if (data[item] === null) data[item] = ''
  }
  return data
}
