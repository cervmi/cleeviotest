export function getParamFromUrl(name: string): string | null {
  return new URL(window.location.href).searchParams.get(name)
}
