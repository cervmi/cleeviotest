import { IStringObject } from '../types/IStringObject'

export function mergeArrayOfObjects<T = IStringObject, U = IStringObject>(mainArray: T[], arrayExtension: U[], key: string) {
  if (!mainArray || !arrayExtension || !key) return []

  for (const [i, value] of mainArray.entries()) {
    const item2 = arrayExtension.find((x: IStringObject) => x[key] === (value as IStringObject)[key])
    if (item2) {
      mainArray[i] = { ...mainArray[i], ...item2 }
    }
  }
  return mainArray
}
