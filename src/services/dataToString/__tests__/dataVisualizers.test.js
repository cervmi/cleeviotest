import { formatDateForDisplayShort, formatDateTimeForDisplayLong } from '../dateTimeFormatter'
import { setCurrentTimeZone } from '../../functions/timezones'
import moment from 'moment'

describe('Data visualisation functions tests', () => {
  test('formatting of date', async () => {
    const timestamp = '2019-05-27T07:33:38'
    setCurrentTimeZone('UTC')

    moment.locale('en-us')

    expect(formatDateForDisplayShort(timestamp)).toBe('05/27/2019')
    expect(formatDateTimeForDisplayLong(timestamp)).toBe('May 27, 2019 7:33 AM (UTC +00:00)')

    moment.locale('cs')

    expect(formatDateForDisplayShort(timestamp)).toBe('27.05.2019')
    expect(formatDateTimeForDisplayLong(timestamp)).toBe('27. květen 2019 7:33 (UTC +00:00)')
  })
})
