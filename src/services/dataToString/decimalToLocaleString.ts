export function decimalToLocaleString(value: number): string {
  return Number(value).toLocaleString('EN-US')
}

export function moneyToLocaleString(value: number, currency?: string): string {
  if (!currency) return Number(value).toLocaleString('EN-US')
  return Number(value).toLocaleString('EN-US', { style: 'currency', currency: currency })
}
