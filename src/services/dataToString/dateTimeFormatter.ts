import moment from 'moment'

export function formatDateTimeForDisplayLong(dateToFormat: string | Date) {
  if (!dateToFormat) return null
  const datetimeToShow = moment(dateToFormat)
  return `${datetimeToShow.format('LLL')} (UTC ${datetimeToShow.format('Z')})`
}

export function formatDateTimeForDisplayShort(dateToFormat: string | Date) {
  if (!dateToFormat) return null
  const datetimeToShow = moment(dateToFormat)
  return datetimeToShow.format('l') + ' ' + datetimeToShow.format('LTS')
}

export function formatDateForDisplayShort(dateToFormat: string | Date) {
  if (!dateToFormat) return null
  return moment(dateToFormat).format('L')
}
