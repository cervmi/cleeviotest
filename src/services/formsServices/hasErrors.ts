import { RefsHash } from 'valuelink'

export function hasErrors(...valueLinks: RefsHash[]) {
  for (const linkObject of valueLinks) {
    for (const link in linkObject) {
      if (linkObject[link].error) return true
    }
  }
  return false
}
