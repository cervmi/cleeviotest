/* istanbul ignore file */
export interface IStringObject<T = any> {
  [key: string]: T
}
