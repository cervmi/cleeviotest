/* istanbul ignore file */
import { sortArrayBy } from '../functions/sortArrayBy'
import { MouseEventHandler } from 'react-select/src/types'
import { IStringObject } from './IStringObject'
import * as React from 'react'

export interface IReactSelectItem<DT = IReactSelectData> {
  //same as  OptionProps from react-select/src/types
  label: string
  value: any
  data?: DT
  id?: number
  index?: number
  isDisabled?: boolean
  isFocused?: boolean
  isSelected?: boolean
  onClick?: MouseEventHandler
  onMouseOver?: MouseEventHandler
  innerRef?: React.Ref<any>
}

export interface IReactSelectData extends IStringObject {
  style?: { color?: string; fontWeight?: string }
}

export type IReactSelectGroup<DT = IReactSelectData> = Array<{
  label: string
  options: IReactSelectItem<DT>[]
}>

export const reactSelectItemStyleCustomRenderer = {
  option: (styles: any, { data }: { data: IReactSelectItem }) => {
    if (data.data?.style)
      return {
        ...styles,
        ...data.data.style
      }
    else return styles
  }
}

export function sortReactSelectItemsByLabel<T = IReactSelectData>(data: IReactSelectItem<T>[]): IReactSelectItem<T>[] {
  if (!data || data.length <= 0) return []
  return sortArrayBy(data, x => x.label)
}
