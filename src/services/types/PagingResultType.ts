/* istanbul ignore file */

export type PagingResultType<T> = {
  pageInfo: pageInfoType
  edges: T[]
}

export type pageInfoType = {
  startCursor: string
  endCursor: string
}
