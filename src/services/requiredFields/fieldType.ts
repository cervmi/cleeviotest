/* istanbul ignore file */

import { fieldNameConversion } from '../fields/fieldNameConversion'

export enum FieldKind {
  packet = 'PACKET',
  product = 'PRODUCT'
}

export function convertToFieldKind(stringKind: string) {
  return FieldKind[fieldNameConversion(stringKind) as keyof typeof FieldKind]
}

export enum FieldType {
  allCountry = 'ALL_COUNTRY',
  allCurrency = 'ALL_CURRENCY',
  boolean = 'BOOLEAN',
  branch = 'BRANCH',
  carrier = 'CARRIER',
  country = 'COUNTRY',
  currency = 'CURRENCY',
  date = 'DATE',
  decimal = 'DECIMAL',
  econtHolidayDeliveryDay = 'ECONT_HOLIDAY_DELIVERY_DAY',
  customsDeclarationType = 'CUSTOMS_DECLARATION_TYPE',
  customsDuty = 'CUSTOMS_DUTY',
  email = 'EMAIL',
  id = 'ID',
  length = 'LENGTH',
  money = 'MONEY',
  quantity = 'QUANTITY',
  string = 'STRING',
  timezone = 'TIMEZONE'
}

export function convertToFieldType(stringType: string) {
  return FieldType[fieldNameConversion(stringType) as keyof typeof FieldType]
}

export enum FieldValidation {
  isNumber = 'isNumber',
  isInteger = 'isInteger',
  isPositive = 'isPositive',
  lessThen = 'lessThen',
  moreThen = 'moreThen',
  lessThenOrEqual = 'lessThenOrEqual',
  moreThenOrEqual = 'moreThenOrEqual',
  isEmail = 'isEmail',
  isPhone = 'isPhone',
  maxLength = 'maxLength',
  minLength = 'minLength',
  maxDate = 'maxDate',
  minDate = 'minDate',
  isDate = 'isDate',
  isTrue = 'isTrue',
  dateIgnoreWeekends = 'dateIgnoreWeekends'
}

export function convertToFieldValidation(stringValidation: string) {
  return FieldValidation[fieldNameConversion(stringValidation) as keyof typeof FieldValidation]
}
