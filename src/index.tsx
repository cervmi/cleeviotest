/* istanbul ignore file */
import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'
import React from 'react'
import ReactDOM from 'react-dom'
import * as serviceWorker from './services/appConfiguration/registerServiceWorker'
import Main from './Main'

ReactDOM.render(<Main />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()

// @ts-ignore
if (module.hot) {
  // @ts-ignore
  module.hot.accept()
}
