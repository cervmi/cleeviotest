/* istanbul ignore file */
import React, { useEffect } from 'react'
import App from './App'
import ErrorBoundary from './components/Error/errorBoundary'
import { useTranslation } from 'react-i18next'
import moment from 'moment'

export default function Main() {
  /*set global moment language - used for react-dates datepicker and displaying time and date*/
  const { i18n } = useTranslation()
  useEffect(() => {
    moment.locale(i18n.language)
  }, [i18n.language])

  return (
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  )
}
