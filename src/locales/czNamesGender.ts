/**
 * Gender setup for keys in Czech language.
 *
 * Mainly for keys in formFields namespace in connection with form validation error messages,
 * which has to have different shape for different gender.
 *
 */
export const czNamesGender: any = {
  company: 'female',
  parcelWeightKg: 'female',
  email: 'male',
  country: 'female',
  currency: 'female',
  moneyCurrency: 'female',
  moneyCod: 'female',
  customsDuty: 'female',
  carrier: 'male',
  sender: 'male'
}
