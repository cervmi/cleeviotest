import React, { lazy, Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import LoadingContainer from './components/LoadingContainer'
import MasterPage from './components/MasterPage/masterPage'
import './css/app.scss'
import './css/Bootstrap/bootstrap.scss'
import './css/dashboard.scss'

import { withMasterPageForPublic, withPageContainer } from './components/MasterPage/masterPageHOC'

import P404 from './components/Error/404'
import PublicMain from './scenesPublic/LandingPage'
import Login from './scenesPublic/Login/login'

const Register = lazy(() => import('./scenesPublic/Register/register'))
const PwdResetRequest = lazy(() => import('./scenesPublic/Login/pwdResetRequest'))
const PwdReset = lazy(() => import('./scenesPublic/Login/pwdReset'))

/* istanbul ignore next */
function EmptyPage() {
  return <></>
}

/* istanbul ignore next */
function Loading() {
  return (
    <MasterPage isForPublic isForAuthenticatedUser>
      <div className="my-3 my-md-4">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <LoadingContainer showLoader />
            </div>
          </div>
        </div>
      </div>
    </MasterPage>
  )
}

export default function App() {
  return (
    <BrowserRouter>
      <Suspense fallback={<Loading />}>
        <Switch>
          {/* Public pages */}
          <Route exact path="/" component={withMasterPageForPublic(withPageContainer(PublicMain))} />
          {/*Used for reloading same page from horizontal menu */}
          <Route path="/reload" component={withMasterPageForPublic(withPageContainer(EmptyPage))} key="reload" />
          <Route path="/login" component={withMasterPageForPublic(withPageContainer(Login))} />
          <Route path="/register" component={withMasterPageForPublic(withPageContainer(Register))} />
          <Route path="/password-reset-request" component={withMasterPageForPublic(withPageContainer(PwdResetRequest))} />
          <Route path="/reset" component={withMasterPageForPublic(withPageContainer(PwdReset))} />

          {/* Fail save */}
          <Route component={P404} />
        </Switch>
      </Suspense>
    </BrowserRouter>
  )
}
